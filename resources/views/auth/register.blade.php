@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <h2>Registrar</h2>
    </div>

    <div class="row">
        <form action="{{ route('register') }}" method="POST">
            {{ csrf_field() }}
            <div class="input-field col s8">
                <input type="text" name="username" required autofocus value="{{ old('username') }}" autocomplete="off">
                <label for="username">Username</label>
                @if ($errors->has('username'))
                    <blockquote>{{$errors->first('username')}}</blockquote>
                @endif
            </div>

            <div class="input-field col s8">
                <input type="text" name="first_name" required autofocus value="{{ old('first_name') }}" autocomplete="off">
                <label for="first_name">Nombre</label>
                @if ($errors->has('first_name'))
                    <blockquote>{{$errors->first('first_name')}}</blockquote>
                @endif
            </div>

            <div class="input-field col s8">
                <input type="text" name="last_name" required autofocus value="{{ old('last_name') }}" autocomplete="off">
                <label for="last_name">Apellidos</label>
                @if ($errors->has('last_name'))
                    <blockquote>{{$errors->first('last_name')}}</blockquote>
                @endif
            </div>

            <div class="input-field col s8">
                <input type="email" name="email" required autofocus value="{{ old('email') }}" class="validate" autocomplete="off">
                <label for="email">E-mail</label>
                @if ($errors->has('email'))
                    <blockquote>{{$errors->first('email')}}</blockquote>
                @endif
            </div>

            <div class="input-field col s8">
                <input type="password" name="password" required autofocus value="{{ old('password') }}">
                <label for="password">Contraseña</label>
                @if ($errors->has('password'))
                    <blockquote>{{$errors->first('password')}}</blockquote>
                @endif
            </div>

            <div class="input-field col s8">
                <input type="password" name="password_confirmation" required autofocus value="{{ old('password_confirmation') }}">
                <label for="password_confirmation">Confirmar Contraseña</label>
                @if ($errors->has('password_confirmation'))
                    <blockquote>{{$errors->first('password_confirmation')}}</blockquote>
                @endif
            </div>
            <div class="input-field col s6 right">
                <button class="btn waves-effect waves-light" type="submit">
                Registrar <i class="material-icons right">send</i>
                </button>
            </div>
            

        </form>
    </div>
</div>
@endsection
