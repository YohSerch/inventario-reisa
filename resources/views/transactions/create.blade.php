@extends('layouts.app')

@section('content')

	<div class="container">
		<h2>Añadir movimiento de inventario ({{$product->nombre}})</h2>
		<div class="row">
			<form action="{{ route('transactions.store', ['product' => $product->id]) }}" method="POST">
				{{ csrf_field() }}
				
				<div class="input-field col s8">
					<select name="type" required>
						<option value="" disabled selected>Selecciona una opción</option>
						<option value="1">Entrada de producto</option>
						<option value="0">Salida de producto</option>
				    </select>
				    <label>Tipo de movimiento</label>
				</div>
				@if ($errors->has('type'))
                        <div class="card-panel red">
                            <span class="white-text">
                                {{$errors->first('type')}}
                            </span>
                        </div>
                    @endif
				
				<div class="input-field col s8">
					<input type="number" name="modified_quantity" value="{{ old('modified_quantity') }}" class="validate" min="1" required>
					<label for="modified_quantity">Cantidad a modificar</label>
					@if ($errors->has('modified_quantity'))
                        <div class="card-panel red">
                            <span class="white-text">
                                {{$errors->first('modified_quantity')}}
                            </span>
                        </div>
                    @endif
				</div>

				<div class="panel-card orange white-text col s4 hoverable thin">
						<i class="material-icons left medium">error</i>
						Si el movimiento es de <strong>Salida de producto</strong> recuerda que la cantidad no debe ser mayor a nuestro stock <strong>{{$product->quantity}}</strong>
				</div>

				<div class="input-field col s8">
					<input type="text" name="purchase_at" value="{{ old('purchase_at') }}" class="datepicker">
					<label for="purchase_at">Fecha de compra(Opcional)</label>
					@if ($errors->has('purchase_at'))
                        <div class="card-panel red">
                            <span class="white-text">
                                {{$errors->first('purchase_at')}}
                            </span>
                        </div>
                    @endif
				</div>

				<div class="input-field col s8">
					<input type="text" name="provider" value="{{ old('provider') }}">
					<label for="provider">Provedor (Opcional)</label>
					@if ($errors->has('provider'))
                        <div class="card-panel red">
                            <span class="white-text">
                                {{$errors->first('provider')}}
                            </span>
                        </div>
                    @endif
				</div>

				<div class="input-field col s8">
					<textarea name="addressee" id="addressee" class="materialize-textarea"></textarea>
					<label for="addressee">Destinatario(Opcional)</label>
					@if ($errors->has('addressee'))
                        <div class="card-panel red">
                            <span class="white-text">
                                {{$errors->first('addressee')}}
                            </span>
                        </div>
                    @endif
				</div>

				<div class="input-field col s8">
					<button type="submit" class="waves-effect waves-light btn">
						Guardar movimiento
						<i class="material-icons right">send</i>
					</button>
				</div>
			</form>
		</div>
	</div>

@endsection

@section('mini_scripts')
	<script>
		$(document).ready(function() {
			$('select').material_select();

			$('.datepicker').pickadate({
				// Strings and translations
				monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
				monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dec'],
				weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
				weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
			    selectMonths: true, // Creates a dropdown to control month
			    selectYears: 15, // Creates a dropdown of 15 years to control year,
			    today: 'Hoy',
			    clear: 'Limpiar',
			    close: 'Ok',
			    closeOnSelect: false, // Close upon selecting a date,
			    format: 'yyyy-mm-dd'
			  });
		});
	</script>
@endsection