@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <h2>Login</h2>
        
    </div>

    <div class="row">
        <form action="{{ route('login') }}" method="POST">
            {{ csrf_field() }}

            <div class="input-field col s8">
                <label for="username">Username</label>
                <input type="text" name="username" value="{{ old('email') }}" required autofocus autocomplete="off">
                @if ($errors->has('username'))
                    <div class="card-panel red">
                        <span class="white-text">
                            {{$errors->first('username')}}
                        </span>
                    </div>
                @endif
            </div>

            <div class="input-field col s8">
                <label for="password">Contraseña</label>
                <input type="password" name="password" required>
                @if ($errors->has('password'))
                    <div class="card-panel red">
                        <span class="white-text">
                            {{$errors->first('password')}}
                        </span>
                    </div>
                @endif
            </div>

            <div class="col s8">
                <p>
                    <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} class="filled-in">
                    <label for="remember">Recuerdame</label>
                </p>
            </div>
            <div class="input-field col s8">
                <a href="{{ route('password.request') }}" class="waves-effect waves-light btn">¿Olvidaste tu contraseña?</a>
                <button class="btn waves-effect waves-light" type="submit">
                    Entrar
                    <i class="material-icons right">send</i>
                </button>
            </div>
        </form>
    </div>
</div>

@endsection
