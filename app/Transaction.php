<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
use App\User;
use Carbon\Carbon;


class Transaction extends Model
{

	public function getCreatedAtAttribute($value) {
		return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d-m-Y H:i');
	}

	public function getPurchaseAtAttribute($value) {
		if ($value) {
			return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d-m-Y');
		}
		return $value;
	}

    public function product() {
    	return $this->belongsTo(Product::class);
    }

    public function user() {
    	return $this->belongsTo(User::class);
    }
}
