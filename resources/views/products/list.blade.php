@extends('layouts.app')

@section('content')
	<div class="container">
		<h2>Listado de productos</h2>

		<div class="row">
			<table class="centered responsive-table striped">
				<thead>
					<tr>
						<th>Código de Producto</th>
						<th>Marca</th>
						<th>Descripción</th>
						<th>Inventario</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($products as $product)
						<tr>
							<td>{{ $product->nombre }}</td>
							<td>{{ $product->mark }}</td>
							<td>{{ $product->description }}</td>
							<td>{{ $product->quantity }}</td>
							<td>
								<a href="{{ route('products.edit', ['product' => $product->id]) }}" class="tooltipped amber-text" data-position="right" data-delay="30" data-tooltip="Editar producto"><i class="material-icons">edit</i></a>
								<a href="{{ route('transactions.create', ['product' => $product->id]) }}" class="tooltipped green-text" data-position="right" data-delay="30" data-tooltip="Modificar inventario"><i class="material-icons">sync</i></a>
								<a href="{{ route('transactions.index', ['product' => $product->id]) }}" class="tooltipped blue-text" data-position="right" data-delay="30" data-tooltip="Historico del producto"><i class="material-icons">history</i></a>
								<form action="{{ route('products.destroy', ['product' => $product->id])}}" method="POST" style="display: inline-block;">
									{{ csrf_field() }}
									<input type="hidden" name="_method" value="DELETE">
									<a href="#" onclick="this.parentNode.submit();" class="tooltipped red-text" data-position="right" data-delay="30" data-tooltip="Eliminar producto"><i class="material-icons">delete</i></a>
								</form>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	
	<div class="row center-align">
		{{$products->links()}}
	</div>


	<div class="fixed-action-btn">
		<a href="{{ route('products.export') }}" target="_blank" class="tooltipped btn-floating btn-large red lighten-1" data-position="left" data-tooltip="Exportar a excel">
			<i class="large material-icons">file_download</i>
		</a>
		<a href="{{ route('products.create') }}" class="tooltipped btn-floating btn-large blue" data-position="left" data-tooltip="Agregar producto">
			<i class="large material-icons">add</i>
		</a>
	</div>
@endsection

@section('mini_scripts')
	@if(session('message'))
        <script>
            Materialize.toast('{{ session("message") }}', 4000, 'teal lighten-2 white-text');
        </script>
    @endif
@endsection