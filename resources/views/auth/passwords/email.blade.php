@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h2>Restablecer contraseña</h2>
        </div>
        <div class="row">
            <form action="{{ route('password.email') }}" method="POST">
                {{ csrf_field() }}
                <div class="input-field col s8">
                    <input type="email" name="email" value="{{ old('email') }}" required autofocus autocomplete="off">
                    <label for="email">Correo eléctronico</label>
                    @if ($errors->has('password'))
                        <div class="card-panel red">
                            <span class="white-text">
                                {{$errors->first('email')}}
                            </span>
                        </div>
                    @endif
                </div>
                <div class="input-field col s8">
                    <button class="waves-effect waves-light btn" type="submit">
                        Enviar link para restablecer contraseña
                        <i class="material-icons right">send</i>
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('mini_scripts')
    @if(session('status'))
        <script>
            Materialize.toast('{{ session("status") }}', 4000, 'teal lighten-2 white-text');
        </script>
    @endif
@endsection
