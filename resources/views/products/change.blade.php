@extends('layouts.app')

@section('content')

	<div class="container">
		<h2>Editar producto</h2>
		<div class="row">
			<form action="{{ route('products.update', ['product' => $product->id]) }}" method="POST">
				{{ csrf_field() }}
				<input type="hidden" name="_method" value="PUT">
				<div class="input-field col s8">
					<input type="text" name="nombre" value="{{ $product->nombre or old('nombre') }}" class="validate" required autofocus>
					<label for="nombre">Código de producto</label>
				</div>

				<div class="input-field col s8">
					<input type="text" name="mark" value="{{ $product->mark or old('mark') }}" class="validate" required>
					<label for="mark">Marca del producto</label>
				</div>

				<div class="input-field col s8">
					<textarea name="description" id="description" class="materialize-textarea">{{$product->description or old('description')}}</textarea>
					<label for="description">Descripción</label>
				</div>

				<div class="input-field col s8">
					<button type="submit" class="waves-effect waves-light btn">
						Actualizar producto
						<i class="material-icons right">send</i>
					</button>
				</div>
			</form>
		</div>
	</div>

@endsection