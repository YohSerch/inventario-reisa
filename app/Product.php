<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Transaction;

class Product extends Model
{
    
	protected $fillable = ['nombre', 'mark', 'description', 'quantity'];

    public function transactions() {
    	return $this->hasMany(Transaction::class);
    }
}
