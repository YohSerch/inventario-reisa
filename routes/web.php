<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::resource('products', 'ProductController', ['except' => ['show']])->middleware('auth');

Route::middleware(['auth'])->group(function() {
	Route::get('/', 'ProductController@index');
	Route::get('products/{product}/transactions/create', 'TransactionController@create')->name('transactions.create');
	Route::post('products/{product}/transactions', 'TransactionController@store')->name('transactions.store');
	Route::get('products/{product}/transactions', 'TransactionController@index')->name('transactions.index');
	Route::get('products/search', 'ProductController@search')->name('products.search');
	Route::get('products/export', 'ProductController@export')->name('products.export');
});
