# Sistema de control de inventario

__Tecnologías usadas:__
+ Laravel 5.5
+ Materializecss
+ [Laravel Excel 2.1](https://github.com/Maatwebsite/Laravel-Excel/tree/2.1)

__Funciones:__

+ Registro y Login de usurios
+ Recuperacion de password
+ CRUD de productos
+ Registro de transacciones asignada al usuario que la realiza
+ Notificación por email de cada transacción realizada
+ Export a Excel del inventario actual