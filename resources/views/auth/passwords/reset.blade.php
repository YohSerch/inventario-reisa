@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h2>Reestablecer Contraseña</h2>
        </div>
        <div class="row">
            <form action="{{ route('password.request') }}" method="POST">
                {{ csrf_field() }}

                <input type="hidden" name="token" value="{{ $token }}">

                <div class="input-field col s8">
                    <input type="email" name="email" value="{{ $email or old('email') }}" required autofocus>
                    <label for="email">Correo Eléctronico</label>
                    @if ($errors->has('password'))
                        <div class="card-panel red">
                            <span class="white-text">
                                {{$errors->first('email')}}
                            </span>
                        </div>
                    @endif
                </div>

                <div class="input-field col s8">
                    <input type="password" name="password" required>
                    <label for="password">Contraseña</label>
                    @if ($errors->has('password'))
                        <div class="card-panel red">
                            <span class="white-text">
                                {{$errors->first('password')}}
                            </span>
                        </div>
                    @endif
                </div>

                <div class="input-field col s8">
                    <input type="password" name="password_confirmation" required>
                    <label for="password_confirmation">Confirmar contraseña</label>
                    @if ($errors->has('password'))
                        <div class="card-panel red">
                            <span class="white-text">
                                {{$errors->first('password_confirmation')}}
                            </span>
                        </div>
                    @endif
                </div>

                <div class="input-field col s8">
                    <button type="submit" class="waves-effect waves-light btn">
                        Restablecer contraseña
                        <i class="material-icons right">send</i>
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
