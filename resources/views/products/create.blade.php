@extends('layouts.app')

@section('content')

	<div class="container">
		<h2>Crear nuevo producto</h2>
		<div class="row">
			<form action="{{ route('products.store') }}" method="POST">
				{{ csrf_field() }}
				<div class="input-field col s8">
					<input type="text" name="nombre" value="{{ old('nombre') }}" class="validate" required autofocus>
					<label for="nombre">Código de producto</label>
					@if ($errors->has('nombre'))
                        <div class="card-panel red">
                            <span class="white-text">
                                {{$errors->first('nombre')}}
                            </span>
                        </div>
                    @endif
				</div>

				<div class="input-field col s8">
					<input type="text" name="mark" value="{{ old('mark') }}" class="validate" required>
					<label for="mark">Marca del producto</label>
				</div>

				<div class="input-field col s8">
					<textarea name="description" id="description" class="materialize-textarea"></textarea>
					<label for="description">Descripción</label>
				</div>

				<div class="input-field col s8">
					<input type="number" name="quantity" value="{{ old('quantity') }}" class="validate" min="0" required>
					<label for="quantity">Cantidad en inventario</label>
				</div>

				<div class="input-field col s8">
					<button type="submit" class="waves-effect waves-light btn">
						Guardar producto
						<i class="material-icons right">send</i>
					</button>
				</div>
			</form>
		</div>
	</div>

@endsection