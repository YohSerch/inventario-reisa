<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;


use App\Product;
use App\Transaction;
use App\Mail\TransactionOutput;
use App\Mail\TransactionEntry;
use Carbon\Carbon;



class TransactionController extends Controller {

    public function index(Request $request, Product $product) {
        $transactions = Transaction::where('product_id', $product->id)->orderBy('created_at', 'desc')->paginate(8);
        return view('transactions.index', ['transactions' => $transactions, 'product' => $product]);
    }

    public function create(Request $request, Product $product) {
        return view('transactions.create', ['product' => $product]);
    }

    public function store(Request $request, Product $product) {
        if ($request->type == 0) {
            $quantity = 'required|integer|min:1|max:'.$product->quantity;
        } else {
            $quantity = 'required|integer|min:1';
        }

        $this->validate($request, [
            'type' => 'required|boolean',
            'modified_quantity' => $quantity,
            'addressee'=> 'nullable',
            'purchase_at' => 'nullable|date:Y-m-d',
            'provider' => 'nullable'
        ]);

        $transaction = new Transaction;
        $transaction->user()->associate($request->user());
        $transaction->modified_quantity = $request->modified_quantity;
        $transaction->addressee = $request->addressee;
        $transaction->type = $request->type;
        if ($request->purchase_at) {
            $transaction->purchase_at = Carbon::createFromFormat('Y-m-d', $request->purchase_at)->format('Y-m-d H:i:s');
        } else {
            $transaction->purchase_at = $request->purchase_at;
        }
        $transaction->provider = $request->provider;

        $product->transactions()->save($transaction);

        if ($transaction->type == 1) {
            $product->quantity = $product->quantity + $transaction->modified_quantity;
        } else {
            $product->quantity = $product->quantity - $transaction->modified_quantity;
        }
        $product->save();

        $this->send_notification($transaction);

        session()->flash('message', 'Transacción guardada con éxito');
        return redirect()->route('transactions.index', ['product' => $product->id]);
    }

    private function send_notification($transaction) {
        $entry = []; // Arreglo de emails a los cuales notificar cuando entra stock en el inventario
        $output = []; // Arreglo de emails a los cuales notificar cuando sale stock del inventario

        if ($transaction->type) {
            Mail::to($entry)->send(new TransactionEntry($transaction));
        } else {
            Mail::to($output)->send(new TransactionOutput($transaction));
        }
    }

}