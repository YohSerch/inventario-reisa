<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $products = Product::orderBy('nombre', 'desc')->paginate(10);
        return view('products.list', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required|unique:products',
            'mark' => 'required',
            'description' => 'nullable',
            'quantity' => 'required|integer|min:0' 
        ]);

        $product = new Product;
        $product->nombre = $request->nombre;
        $product->mark = $request->mark;
        $product->description = $request->description;
        $product->quantity = $request->quantity;
        $product->save();

        session()->flash('message', 'Producto agregado con éxito');
        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product) {       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product) {
        return view('products.change', ['product' => $product]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product) {
        $this->validate($request, [
            'nombre' => 'required',
            'mark' => 'required',
            'description' => 'nullable'
        ]);

        $product->nombre = $request->nombre;
        $product->mark = $request->mark;
        $product->description = $request->description;
        $product->save();
        session()->flash('message', 'Producto actualizado con éxito');
        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product) {
        $product->delete();

        session()->flash('message', 'Producto eliminado con éxito');
        return redirect()->route('products.index');
    }

    
    public function search(Request $request) {
        $products = Product::where('nombre', 'like', '%'.$request->s.'%')
                             ->orWhere('mark', 'like', '%'.$request->s.'%')
                             ->orWhere('description', 'like', '%'.$request->s.'%')
                             ->paginate(10);
        return view('products.list', ['products' => $products]);
    }


    public function export(Request $request) {
        Excel::create('inventario_reisa', function($excel) {
            
            $excel->setTitle('Inventario Reisa');

            $excel->setCreator('Sergio Durán')
                  ->setCompany('Reisa Tecnología');

            $excel->setDescription('Listado de productos dentro del stock de Reisa');
            
            $excel->getDefaultStyle()
                  ->getAlignment()
                  ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                  ->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);

            $excel->sheet('Inventario', function($sheet) {
                $products = Product::all(['nombre', 'mark', 'description', 'quantity', 'created_at']);
                $sheet->fromModel($products);

                $sheet->cells('A1:E1', function($cells) {
                    $cells->setBackground('#42a5f5');
                    $cells->setfontColor('#FFFFFF');
                    $cells->setfontSize(16);
                    $cells->setAlignment('center');
                     $cells->setValignment('center');
                });

                $sheet->cell('A1', function($cell) {
                    $cell->setValue('Código de producto');
                });

                $sheet->cell('B1', function($cell) {
                    $cell->setValue('Marca');
                });
                $sheet->cell('C1', function($cell) {
                    $cell->setValue('Descripción');
                });
                $sheet->cell('D1', function($cell) {
                    $cell->setValue('Cantidad en stock');
                });
                $sheet->cell('E1', function($cell) {
                    $cell->setValue('Fecha de registro');
                });
                $sheet->setHeight(1, 20);

                for($i = 1; $i <= count($products) + 1; $i++) {
                    $sheet->setHeight($i, 20);
                }

                $sheet->setColumnFormat(array(
                    'E' => 'dd/mm/yy',
                ));

                $sheet->setAutoSize(true);
            });


        })->download('xlsx');
    } 
}
