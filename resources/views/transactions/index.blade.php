@extends('layouts.app')

@section('content')
	<div class="container">
		<h2>Historial de movimientos ({{$product->nombre}})</h2>
		<div class="row">
			<table class="centered responsive-table">
				<thead>
					<tr>
						<th>Tipo</th>
						<th>Cantidad</th>
						<th>Fecha de registro</th>
						<th>Fecha de compra</th>
						<th>Provedor</th>
						<th>Destinatario</th>
						<th>Modifico</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($transactions as $transaction)
						<tr class="{{$transaction->type ? 'green accent-1' : 'red accent-1 white-text' }}">
							<td>
								@if ($transaction->type)
									<i class="material-icons">expand_less</i>
								@else
									<i class="material-icons">expand_more</i>
								@endif
							</td>
							<td>{{ $transaction->modified_quantity }}</td>
							<td>{{ $transaction->created_at }}</td>
							<td>{{ $transaction->purchase_at }}</td>
							<td>{{ $transaction->provider }}</td>
							<td>{{ $transaction->addressee }}</td>
							<td>{{ $transaction->user->first_name }}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="row center-align">
			{{$transactions->links()}}
		</div>
	</div>
@endsection

@section('mini_scripts')
	@if(session('message'))
        <script>
            Materialize.toast('{{ session("message") }}', 4000, 'teal lighten-2 white-text');
        </script>
    @endif
@endsection