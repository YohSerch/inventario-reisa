<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons|Roboto" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>

    <nav class="nav-extended">
        <div class="nav-wrapper blue-grey darken-4">
            <a href="{{ url('/') }}" class="brand-logo reisa">{{ config('app.name', 'Inventario Reisa') }}</a>
            <ul class="right hide-on-med-and-down">
                @guest
                    <li><a href="{{ url('login') }}">Login</a></li>
                    <li><a href="{{ url('register') }}">Registrar</a></li>
                @else
                    <li><a href="{{ route('products.index') }}">Inventario</a></li>
                    <li><a class="dropdown-button" href="#!" data-activates="dropdown1">Bienvenido, {{Auth::user()->first_name}}<i class="material-icons right">arrow_drop_down</i></a></li>
                @endguest
            </ul>
            <ul id="dropdown1" class="dropdown-content">
                <li>
                    <a href="{{route('logout')}}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Salir</a>
                </li>
            </ul>

            <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </div>
        <div class="nav-content blue-grey darken-4">
            <form action="{{ route('products.search') }}" method="GET">
                <div class="input-field">
                    <input id="search" type="search" required name="s" style="border-bottom: none; height: 4.5rem; margin: 0; padding-left: 4rem; box-sizing: border-box;" autocomplete="off">
                    <label for="search" class="label-icon"><i class="material-icons">search</i></label>
                    <i class="material-icons">close</i>
                </div>
            </form>
        </div>
    </nav>

    @yield('content')

    <!-- Scripts -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    @yield('mini_scripts')
</body>
</html>
